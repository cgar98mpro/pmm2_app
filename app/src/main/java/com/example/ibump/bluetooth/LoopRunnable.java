package com.example.ibump.bluetooth;

public class LoopRunnable implements Runnable {

    private final RunnableLoopCallback callback;
    private boolean loopEnd;

    public LoopRunnable(RunnableLoopCallback callback, boolean loopEnd) {
        this.callback = callback;
        this.loopEnd = loopEnd;
    }

    @Override
    public void run() {
        do {
            callback.loopCallback();
        } while(!loopEnd);
    }

    public void stop() {
        loopEnd = true;
    }

    public interface RunnableLoopCallback {
        void loopCallback();
    }

}

