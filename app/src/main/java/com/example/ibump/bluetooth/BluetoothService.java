package com.example.ibump.bluetooth;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class BluetoothService {

    private final BluetoothSocket socket;
    private final InputStream inStream;
    private final OutputStream outStream;

    public BluetoothService(BluetoothSocket socket) {

        this.socket = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        //Get the input/output streams
        try {
            tmpIn = socket.getInputStream();
        } catch (IOException e) {
            Log.e("BT_IN_CREATE", "Error occurred when creating input stream", e);
        }
        try {
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
            Log.e("BT_OUT_CREATE", "Error occurred when creating output stream", e);
        }

        //Assign to final
        inStream = tmpIn;
        outStream = tmpOut;

    }

    public void listen(DataReadyCallback callback) {
        byte[] mmBuffer = new byte[1024];
        try {
            int numBytes = inStream.read(mmBuffer);   //Blocking read
            String data = new String(Arrays.copyOfRange(mmBuffer, 0, numBytes), StandardCharsets.UTF_8);
            Log.d("BT_READ_VAL", data);
            callback.onDataReady(data);
        } catch (IOException e) {
            Log.d("BT_READ", "Input stream was disconnected", e);
        }
    }

    public void write(byte[] bytes) {
        try {
            outStream.write(bytes);
        } catch (IOException e) {
            Log.e("BT_WRITE", "Error occurred when sending data", e);
        }
    }

    public void cancel() {
        try {
            socket.close();
        } catch (IOException e) {
            Log.e("BT_CLOSE", "Could not close the connect socket", e);
        }
    }

    public interface DataReadyCallback {
        void onDataReady(String data);
    }

}
