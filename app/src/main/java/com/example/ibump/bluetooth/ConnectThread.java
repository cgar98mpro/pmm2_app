package com.example.ibump.bluetooth;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

public class ConnectThread extends Thread {

    private final BluetoothAdapter bluetoothAdapter;
    private final BluetoothSocket socket;
    private final ConnectCallback callback;

    public ConnectThread(BluetoothAdapter bluetoothAdapter, BluetoothDevice device, ConnectCallback callback) {

        this.bluetoothAdapter = bluetoothAdapter;
        this.callback = callback;

        BluetoothSocket tmp = null;
        UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //MUST be this UUID

        try {
            //Get socket
            //tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            Log.e("BT_SOCKET_OPEN", "Socket's create() method failed", e);
        }
        socket = tmp;

    }

    @SuppressLint("NewApi")
    public void run() {

        //Cancel discovery (not required in this case)
        bluetoothAdapter.cancelDiscovery();

        //Try connection 5 times
        int i = 0;
        while(i++ < 5) {
            try {
                socket.connect();
                break;
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                System.out.println("BT ERROR");
                connectException.printStackTrace();
                if(i == 5) {
                    try {
                        socket.close();
                    } catch (IOException closeException) {
                        Log.e("BT_SOCKET_CLOSE", "Could not close the client socket", closeException);
                    }
                    return;
                }
            }
        }

        //Connection ok
        callback.connectionOk(socket);

    }

    public void cancel() {
        try {
            socket.close();
        } catch (IOException e) {
            Log.e("BT_SOCKET_CLOSE", "Could not close the client socket", e);
        }
    }

    public interface ConnectCallback {
        void connectionOk(BluetoothSocket socket);
    }

}
