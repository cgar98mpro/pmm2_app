package com.example.ibump.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;

import java.nio.charset.StandardCharsets;
import java.util.Set;

public class BluetoothManager {

    private static BluetoothManager instance = null;

    public static BluetoothManager getInstance(BluetoothAdapter bluetoothAdapter, Activity activity,
                                               BluetoothCallback callback) {
        if(instance == null) {
            instance = new BluetoothManager(bluetoothAdapter, activity, callback);
        }
        return instance;
    }

    private final BluetoothAdapter bluetoothAdapter;
    private BluetoothService bluetoothService;

    private LoopRunnable runnable;
    private Thread loopThread;

    private BluetoothManager(BluetoothAdapter bluetoothAdapter, Activity activity, BluetoothCallback callback) {
        this.bluetoothAdapter = bluetoothAdapter;
        if(bluetoothAdapter != null) {
            if(bluetoothAdapter.isEnabled()) {
                configBluetooth(callback);
            } else {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(enableBluetooth, 0);
            }
        }
    }

    private void configBluetooth(BluetoothCallback callback) {

        //Get paired services
        bluetoothService = null;
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        //Look for raspberry
        if(pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for(BluetoothDevice device : pairedDevices) {
                if(device.getName().equalsIgnoreCase("raspberrypi")) {
                    ConnectThread ct = new ConnectThread(bluetoothAdapter, device,new ConnectThread.ConnectCallback() {
                        @Override
                        public void connectionOk(BluetoothSocket socket) {
                            bluetoothService = new BluetoothService(socket);
                            callback.onBluetoothReady();
                        }
                    });
                    ct.start();
                    break;
                }
            }
        }

    }

    public void listenData(BluetoothService.DataReadyCallback callback, boolean continuous) {
        if(loopThread != null && loopThread.isAlive()) {
            runnable.stop();
        }
        runnable = new LoopRunnable(new LoopRunnable.RunnableLoopCallback() {
            @Override
            public void loopCallback() {
                //Do something if bluetooth is ok
                if(bluetoothOk()) {
                    bluetoothService.listen(callback);
                } else {
                    runnable.stop();
                }
            }
        }, !continuous);
        loopThread = new Thread(runnable);
        loopThread.start();
    }

    public void sendData(String data) {
        if(bluetoothOk()) {
            bluetoothService.write(data.getBytes(StandardCharsets.UTF_8));
        }
    }

    private boolean bluetoothOk() {
        return bluetoothAdapter != null && bluetoothAdapter.isEnabled() && bluetoothService != null;
    }

    public interface BluetoothCallback {
        void onBluetoothReady();
    }

}
