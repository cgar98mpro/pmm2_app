package com.example.ibump.ui.viajes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.ibump.R;
import com.example.ibump.databinding.FragmentViajesBinding;

public class ViajesFragment extends Fragment {

    private ViajesViewModel viajesViewModel;
    private FragmentViajesBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        viajesViewModel =
                new ViewModelProvider(this).get(ViajesViewModel.class);

        binding = FragmentViajesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        //final TextView textView = binding.textDashboard;
        //dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
        //    @Override
        //    public void onChanged(@Nullable String s) {
        //        textView.setText(s);
        //    }
        //});

        final SearchView search_direccion = (SearchView) root.findViewById(R.id.searchbar_viajes);
        final TextView search_text = (TextView) root.findViewById(R.id.text_viajes);

        search_text.setText("¿A dónde quieres ir?");

        search_direccion.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence query = search_direccion.getQuery();
                search_text.setText("Quieres ir a " + query);
            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}