package com.example.ibump.ui.home;

import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.ibump.R;
import com.example.ibump.bluetooth.BluetoothManager;
import com.example.ibump.bluetooth.BluetoothService;
import com.example.ibump.databinding.FragmentHomeBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;
    private DatabaseReference database;

    private BluetoothManager bluetoothManager;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        //final TextView textView = binding.startText;
        //homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
        //    @Override
        //    public void onChanged(@Nullable String s) {
        //        textView.setText(s);
        //    }
        //});

        database = FirebaseDatabase.getInstance().getReference();

        //Bluetooth preparation
        bluetoothManager = BluetoothManager.getInstance(BluetoothAdapter.getDefaultAdapter(),
                getActivity(), new BluetoothManager.BluetoothCallback() {
                    @Override
                    public void onBluetoothReady() {
                        bluetoothManager.listenData(new BluetoothService.DataReadyCallback() {
                            @Override
                            public void onDataReady(String data) {
                                if(data.startsWith("Distance")) {
                                    float distance = Float.parseFloat(data.split(" ")[1]);
                                    if(distance < 50) {
                                        database.child("distance").push().setValue(distance);
                                    }
                                }
                            }
                        }, true);
                    }
                });

        final Button start_button = root.findViewById(R.id.start_button);
        final TextView start_text = root.findViewById(R.id.start_text);
        final TextView distanceText = root.findViewById(R.id.distanceText);

        start_button.setActivated(false);
        start_button.setText("START");
        start_text.setText("iBump is stopped");

        DatabaseReference distancia = database.child("distance");
        distancia.limitToLast(1).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot sn : snapshot.getChildren()) {
                    distanceText.setText("Last Distance: " + sn.getValue(Float.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                distanceText.setText("Last Distance: NULL");
            }
        });

        start_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (start_button.isActivated()) {
                    start_button.setActivated(false);
                    database.child("sonido").setValue(false);
                    start_button.setText("START");
                    start_text.setText("iBump is stopped");
                } else {
                    start_button.setActivated(true);
                    database.child("sonido").setValue(true);
                    start_button.setText("STOP");
                    start_text.setText("iBump is running!");
                }
            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}